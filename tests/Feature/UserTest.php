<?php

namespace Tests\Feature;

use Tests\TestCase;
use Skygard\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Skygard\Client;
use Skygard\Testing\OAuth;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Setup user and client
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->client = factory(Client::class)->create();

        OAuth::actingAs($this->user)->withClient($this->client);
    }

    /**
     * Check user authenticates.
     *
     * @return void
     */
    public function testUserIsAuthenticated()
    {
        $this->json('GET', '/api/user')
            ->assertStatus(200)
            ->assertJson([
                'name' => $this->user->name,
                'email' => $this->user->email
            ]);
    }
}
