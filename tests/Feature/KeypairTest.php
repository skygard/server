<?php

namespace Tests\Feature;

use Skygard\User;
use Skygard\Client;
use Tests\TestCase;
use Skygard\Keypair;
use phpseclib\Crypt\RSA;
use Skygard\Testing\OAuth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class KeypairTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Setup user and client
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->client = factory(Client::class)->create();

        OAuth::actingAs($this->user)->withClient($this->client);
    }

    /**
     * Test user can get a single keypair by id
     *
     * @return void
     */
    public function testUserCanGetSingleKeypair()
    {
        $keypair = $this->user->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('GET', "/api/keypairs/{$keypair->id}")
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $keypair->id,
                    'client' => [
                        'id' => $this->client->id
                    ],
                    'encryption' => [
                        'public_key' => 'foo',
                        'private_key' => 'bar',
                    ],
        
                    'signing' => [
                        'public_key' => 'baz',
                        'private_key' => 'qux',
                    ],
                    'revoked' => false
                ]
            ]);
    }

    /**
     * Test user can not see keys created by other clients
     *
     * @return void
     */
    public function testUserCanNotGetOtherClientsKeypair()
    {
        $newClient = factory(Client::class)->create();

        $keypair = $this->user->keypairs()->create([
            'client_id' => $newClient->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('GET', "/api/keypairs/{$keypair->id}")
            ->assertStatus(404)
            ->assertJsonMissing([
                'id' => $keypair->id
            ]);
    }

    /**
     * Test user can not get other users keypairs
     *
     * @return void
     */
    public function testUserCanNotGetOtherUsersKeypair()
    {
        $newUser = factory(User::class)->create();

        $keypair = $newUser->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('GET', "/api/keypairs/{$keypair->id}")
            ->assertStatus(404)
            ->assertJsonMissing([
                'id' => $keypair->id
            ]);
    }

    /**
     * Test user can revoke own keys
     *
     * @return void
     */
    public function testUserCanRevokeKeys()
    {
        $key = $this->user->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux'
        ]);

        $this->json('DELETE', "/api/keypairs/{$key->id}")
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $key->id,
                    'revoked' => true
                ]
            ]);
    }

    /**
     * Test user can not see revoked keys in listing
     *
     * @return void
     */
    public function testUserCanNotSeeRevokedKeys()
    {
        $key = $this->user->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
            'revoked' => true
        ]);

        $this->json('GET', '/api/keypairs')
            ->assertStatus(200)
            ->assertJsonMissing([
                'id' => $key->id
            ]);
    }

    public function testUserCanNotRevokeOtherClientKeys()
    {
        $newClient = factory(Client::class)->create();

        $key = $this->user->keypairs()->create([
            'client_id' => $newClient->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('DELETE', "/api/keypairs/{$key->id}")
            ->assertStatus(404)
            ->assertJsonMissing([
                'id' => $key->id
            ]);
    }

    public function testUserCanNotRevokeOtherUserKeys()
    {
        $newUser = factory(User::class)->create();

        $keypair = $newUser->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('DELETE', "/api/keypairs/{$keypair->id}")
            ->assertStatus(404)
            ->assertJsonMissing([
                'id' => $keypair->id
            ]);
    }
    
    /**
     * Test the current user can see keys created
     * by the current client.
     *
     * @return void
     */
    public function testUserCanSeeClientKeypairs()
    {
        $key = $this->user->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('GET', '/api/keypairs')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'id' => $key->id,
                        'client' => [
                            'id' => $this->client->id
                        ],
                        'encryption' => [
                            'public_key' => 'foo',
                            'private_key' => 'bar',
                        ],
            
                        'signing' => [
                            'public_key' => 'baz',
                            'private_key' => 'qux',
                        ],
                        'revoked' => false
                    ]
                ]
            ]);
    }

    /**
     * Test the current user can not see keys
     * created by other clients.
     *
     * @return void
     */
    public function testUserCanNotSeeOtherClientKeypairs()
    {
        $newClient = factory(Client::class)->create();

        $key = $this->user->keypairs()->create([
            'client_id' => $newClient->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('GET', '/api/keypairs')
            ->assertStatus(200)
            ->assertJsonMissing([
                'id' => $key->id,
                'encryption' => [
                    'public_key' => 'foo',
                    'private_key' => 'bar',
                ],
    
                'signing' => [
                    'public_key' => 'baz',
                    'private_key' => 'qux',
                ],
            ]);
    }

    /**
     * Test the current user can not see keypairs
     * created by other users.
     *
     * @return void
     */
    public function testUserCanNotSeeOtherUserKeypairs()
    {
        $newUser = factory(User::class)->create();

        $keypair = $newUser->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $this->json('GET', '/api/keypairs')
            ->assertStatus(200)
            ->assertJsonMissing([
                'id' => $keypair->id,
                'encryption' => [
                    'public_key' => 'foo',
                    'private_key' => 'bar',
                ],
    
                'signing' => [
                    'public_key' => 'baz',
                    'private_key' => 'qux',
                ],
            ]);
    }

    /**
     * Check user can create keys.
     *
     * @return void
     */
    public function testUserCanCreateKeypairs()
    {
        $rsa = new RSA;

        $encryptionKeyPair = $rsa->createKey(4096);
        $signingKeyPair = $rsa->createKey(4096);

        $rsa->loadKey($signingKeyPair['privatekey']);

        $sign = implode(':', [
            $encryptionKeyPair['publickey'],
            $encryptionKeyPair['privatekey'],
            $signingKeyPair['publickey'],
            $signingKeyPair['privatekey'],
        ]);

        $response = $this->json('POST', '/api/keypairs', [
            'encryption' => [
                'public_key' => $encryptionKeyPair['publickey'],
                'private_key' => $encryptionKeyPair['privatekey'],
            ],

            'signing' => [
                'public_key' => $signingKeyPair['publickey'],
                'private_key' => $signingKeyPair['privatekey'],
            ],
  
            'signature' => base64_encode(
                $rsa->sign($sign)
            )
        ]);

        $response->assertStatus(201)->assertJson([
            'data' => [
                'client' => [
                    'id' => $this->client->id
                ],
                'encryption' => [
                    'public_key' => $encryptionKeyPair['publickey'],
                    'private_key' => $encryptionKeyPair['privatekey'],
                ],
                'signing' => [
                    'public_key' => $signingKeyPair['publickey'],
                    'private_key' => $signingKeyPair['privatekey'],
                ],
                'revoked' => false
            ]
        ]);
    }

    /**
     * Check user keys must have valid signature
     * 
     * @return void
     */
    public function testUserKeypairMustHaveValidSignature()
    {
        $rsa = new RSA;

        $encryptionKeyPair = $rsa->createKey(4096);
        $signingKeyPair = $rsa->createKey(4096);

        $response = $this->json('POST', '/api/keypairs', [
            'encryption' => [
                'public_key' => $encryptionKeyPair['publickey'],
                'private_key' => $encryptionKeyPair['privatekey'],
            ],
            'signing' => [
                'public_key' => $signingKeyPair['publickey'],
                'private_key' => $signingKeyPair['privatekey'],
            ],
            'signature' => 'this is not valid'
        ]);

        $response->assertJsonValidationErrors(['signature']);
    }

    /**
     * Test passing a limit to keypairs endpoint
     * limits the results
     *
     * @return void
     */
    public function testUserKeypairsLimitWorks()
    {
        factory(Keypair::class, 50)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id
        ]);

        $limit = rand(10, 50);

        $this->json('GET', "/api/keypairs?limit={$limit}")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'per_page' => $limit
                ]
            ]);
    }

    /**
     * Test user keypairs pagination limit can
     * not exceed 100 per page
     *
     * @return void
     */
    public function testUserKeypairsLimitCanNotExceedMax()
    {
        factory(Keypair::class, 500)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id
        ]);

        $this->json('GET', "/api/keypairs?limit=200")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'per_page' => 100
                ]
            ]);
    }

    /**
     * Test passing no limit query string param
     * uses the default limit of 20
     *
     * @return void
     */
    public function testUserKeypairsDefaultLimit()
    {
        factory(Keypair::class, 50)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id
        ]);

        $this->json('GET', "/api/keypairs")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'per_page' => 20
                ]
            ]);
    }
}
