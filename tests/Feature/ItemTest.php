<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use Skygard\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Skygard\Client;
use Skygard\Testing\OAuth;
use phpseclib\Crypt\AES;
use phpseclib\Crypt\RSA;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Testing\File;
use Skygard\Keypair;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Skygard\Item;

class ItemTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Set up the tests
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
        $this->client = factory(Client::class)->create();

        OAuth::actingAs($this->user)->withClient($this->client);
    }

    /**
     * Test user can get a single item by id
     *
     * @return void
     */
    public function testUserCanGetSingleItem()
    {
        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', "/api/items/{$item->id}")
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $item->id,
                    'keypair' => [
                        'id' => $keypair->id
                    ],
                    'client' => [
                        'id' => $this->client->id
                    ],
                    'key' => $item->key,
                    'hash' => $item->hash,
                    'metadata' => $item->metadata,
                    'signature' => $item->signature
                ]
            ]);
    }

    /**
     * Test user can not see single item created by other client
     *
     * @return void
     */
    public function testUserCanNotGetOtherClientsItem()
    {
        $newClient = factory(Client::class)->create();

        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $newClient->id
        ]);

        $item = factory(Item::class)->create([
            'client_id' => $newClient->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', "/api/items/{$item->id}")
            ->assertStatus(404)
            ->assertJsonMissing([
                'id' => $item->id
            ]);
    }

    /**
     * Test user can not get other users item
     *
     * @return void
     */
    public function testUserCanNotGetOtherUsersItem()
    {
        $newUser = factory(User::class)->create();

        $keypair = $newUser->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $newUser->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', "/api/items/{$item->id}")
            ->assertStatus(404)
            ->assertJsonMissing([
                'id' => $item->id
            ]);
    }

    /**
     * Test the current user can get a list of their items
     *
     * @return void
     */
    public function testCanGetItemList()
    {
        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', '/api/items')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'id' => $item->id,
                        'keypair' => [
                            'id' => $keypair->id
                        ],
                        'client' => [
                            'id' => $this->client->id
                        ],
                        'key' => $item->key,
                        'hash' => $item->hash,
                        'metadata' => $item->metadata,
                        'signature' => $item->signature
                    ]
                ]
            ]);
    }

    /**
     * Test the current user can not see items
     * created by other clients.
     *
     * @return void
     */
    public function testUserCanNotSeeOtherClientItems()
    {
        $newClient = factory(Client::class)->create();

        $keypair = $this->user->keypairs()->create([
            'client_id' => $newClient->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        factory(Item::class, 50)->create([
            'client_id' => $newClient->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', '/api/items')
            ->assertStatus(200)
            ->assertJsonMissing([
                'id' => $keypair->id,
                'encryption' => [
                    'public_key' => 'foo',
                    'private_key' => 'bar',
                ],
    
                'signing' => [
                    'public_key' => 'baz',
                    'private_key' => 'qux',
                ],
            ]);
    }

    /**
     * Test the current user can not see items
     * created by other users.
     *
     * @return void
     */
    public function testUserCanNotSeeOtherUserItems()
    {
        $newUser = factory(User::class)->create();

        $keypair = $newUser->keypairs()->create([
            'client_id' => $this->client->id,
            'encryption_public_key' => 'foo',
            'encryption_private_key' => 'bar',
            'signing_public_key' => 'baz',
            'signing_private_key' => 'qux',
        ]);

        factory(Item::class, 50)->create([
            'client_id' => $this->client->id,
            'user_id' => $newUser->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', '/api/items')
            ->assertStatus(200)
            ->assertJsonMissing([
                'id' => $keypair->id,
                'encryption' => [
                    'public_key' => 'foo',
                    'private_key' => 'bar',
                ],
    
                'signing' => [
                    'public_key' => 'baz',
                    'private_key' => 'qux',
                ],
            ]);
    }

    /**
     * Test passing a limit to items endpoint
     * limits the results
     *
     * @return void
     */
    public function testItemListLimitWorks()
    {
        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        factory(Item::class, 50)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $limit = rand(10, 50);

        $this->json('GET', "/api/items?limit={$limit}")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'per_page' => $limit
                ]
            ]);
    }

    /**
     * Test user keypairs pagination limit can
     * not exceed 100 per page
     *
     * @return void
     */
    public function testItemListLimitCanNotExceedMax()
    {
        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        factory(Item::class, 500)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', "/api/items?limit=200")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'per_page' => 100
                ]
            ]);
    }

    /**
     * Test passing no limit query string param
     * uses the default limit of 20
     *
     * @return void
     */
    public function testItemListDefaultLimit()
    {
        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        factory(Item::class, 50)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id
        ]);

        $this->json('GET', "/api/items")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'per_page' => 20
                ]
            ]);
    }

    /**
     * Test items can be viewed when created with different keypairs
     * from the same client and user.
     *
     * @return void
     */
    public function testItemListWithDifferentKeypairs()
    {
        $keypairs = factory(Keypair::class, 50)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        factory(Item::class, 50)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypairs->random()->id
        ]);

        $this->json('GET', "/api/items")
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'total' => 50
                ]
            ]);
    }

    /**
     * Test item can be downloaded
     *
     * @return void
     */
    public function testItemCanBeDownloaded()
    {
        Storage::fake('cloud');

        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        Storage::disk('cloud')->put('foo/bar', 'baz');

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id,
            'disk' => 'cloud',
            'path' => 'foo/bar'
        ]);

        Storage::disk('cloud')->assertExists('foo/bar');

        $filesystem = Mockery::mock(app('filesystem'));
        $filesystem
            ->shouldReceive('temporaryUrl')
            ->with($item->path, Mockery::any())
            ->andReturn('http://example.com/foo/bar');

        Storage::shouldReceive('disk')->with('cloud')->andReturn(
            $filesystem
        );

        $this->get("/api/items/{$item->id}/download")
            ->assertRedirect('http://example.com/foo/bar');
    }

    /**
     * Test user can not download other client items
     *
     * @return void
     */
    public function testUserCanNotDownloadOtherClientItems()
    {
        $newClient = factory(Client::class)->create();

        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $newClient->id
        ]);

        $item = factory(Item::class)->create([
            'client_id' => $newClient->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id,
        ]);

        $this->get("/api/items/{$item->id}/download")
            ->assertStatus(404);
    }

    /**
     * Test user can not download other users items
     *
     * @return void
     */
    public function testUserCanNotDownloadOtherUsersItems()
    {
        $newUser = factory(User::class)->create();

        $keypair = factory(Keypair::class)->create([
            'user_id' => $newUser->id,
            'client_id' => $this->client->id
        ]);

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $newUser->id,
            'keypair_id' => $keypair->id,
        ]);

        $this->get("/api/items/{$item->id}")
            ->assertStatus(404);
    }

    /**
     * Test items can be deleted
     *
     * @return void
     */
    public function testItemCanBeDeleted()
    {
        Storage::fake('cloud');

        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id
        ]);

        Storage::disk('cloud')->put('foo/bar', 'baz');

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id,
            'disk' => 'cloud',
            'path' => 'foo/bar'
        ]);

        Storage::disk('cloud')->assertExists('foo/bar');

        $this->json('DELETE', "/api/items/{$item->id}")
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $item->id,
                    'deleted' => true
                ]
            ]);
        
        Storage::disk('cloud')->assertMissing('foo/bar');
    }

    /**
     * Test items can not delete other client items
     *
     * @return void
     */
    public function testUserCanNotDeleteOtherClientItems()
    {
        Storage::fake('cloud');

        $newUser = factory(User::class)->create();

        $keypair = factory(Keypair::class)->create([
            'user_id' => $newUser->id,
            'client_id' => $this->client->id
        ]);

        Storage::disk('cloud')->put('foo/bar', 'baz');

        $item = factory(Item::class)->create([
            'client_id' => $this->client->id,
            'user_id' => $newUser->id,
            'keypair_id' => $keypair->id,
            'disk' => 'cloud',
            'path' => 'foo/bar'
        ]);

        Storage::disk('cloud')->assertExists('foo/bar');

        $this->json('DELETE', "/api/items/{$item->id}")
            ->assertStatus(404);
        
        Storage::disk('cloud')->assertExists('foo/bar');
    }

    /**
     * Test items can not delete other users items
     *
     * @return void
     */
    public function testUserCanNotDeleteOtherUsersItems()
    {
        Storage::fake('cloud');

        $newClient = factory(Client::class)->create();

        $keypair = factory(Keypair::class)->create([
            'user_id' => $this->user->id,
            'client_id' => $newClient->id
        ]);

        Storage::disk('cloud')->put('foo/bar', 'baz');

        $item = factory(Item::class)->create([
            'client_id' => $newClient->id,
            'user_id' => $this->user->id,
            'keypair_id' => $keypair->id,
            'disk' => 'cloud',
            'path' => 'foo/bar'
        ]);

        Storage::disk('cloud')->assertExists('foo/bar');

        $this->json('DELETE', "/api/items/{$item->id}")
            ->assertStatus(404);
        
        Storage::disk('cloud')->assertExists('foo/bar');
    }

    /**
     * Test an item can be uploaded
     *
     * @return void
     */
    public function testItemCanBeUploaded()
    {
        $rsa = new RSA;

        $encryptionKeyPair = $rsa->createKey(4096);
        $signingKeyPair = $rsa->createKey(4096);

        // Load the public key for encryption
        $rsa->loadKey($encryptionKeyPair['publickey']);

        // Generate key
        $key = Str::random(32);

        // AES encrypt a test file (stub) with the key and sign it
        $cipher = new AES;
        $cipher->setKey($key);

        // Create a fake image file for testing
        $file = UploadedFile::fake()->image('test.jpg');

        // Encrypt the file
        $cipherText = $cipher->encrypt(file_get_contents($file));

        // Get a SHA256 hash of the ciphertext
        $sha256 = hash('sha256', $cipherText);

        // Metadata
        $metadata = json_encode([
            'foo' => 'bar'
        ]);

        // Encrypt metadata
        $metadataCipherText = base64_encode(
            $cipher->encrypt($metadata)
        );

        // Encrypt the key
        $encryptedKey = base64_encode(
            $rsa->encrypt($key)
        );

        // Load the private key for signing
        $rsa->loadKey($signingKeyPair['privatekey']);

        // Generate a string to sign
        $sign = implode(':', [
            $encryptedKey,
            $sha256,
            $metadataCipherText
        ]);

        // Sign the string
        $signature = base64_encode(
            $rsa->sign($sign)
        );

        // Write a temporary file to test uploading
        $tempFile = fopen(tempnam('/tmp', 'skygard-test'), 'w');
        fwrite($tempFile, $cipherText);

        // Save the keypair
        $keyPair = $this->user->keypairs()->create([
            'user_id' => $this->user->id,
            'client_id' => $this->client->id,
            'encryption_public_key' => $encryptionKeyPair['publickey'],
            'encryption_private_key' => $encryptionKeyPair['privatekey'],
            'signing_public_key' => $signingKeyPair['publickey'],
            'signing_private_key' => $signingKeyPair['privatekey']
        ]);

        Storage::fake('cloud');

        $res = $this->json('POST', '/api/items', [
            'keypair_id' => $keyPair->id,
            'key' => $encryptedKey,
            'data' => new File('skygard-file', $tempFile),
            'hash' => $sha256,
            'metadata' => $metadataCipherText,
            'signature' => $signature
        ])->assertStatus(201)->assertJson([
            'data' => [
                'key' => $encryptedKey,
                'hash' => $sha256,
                'metadata' => $metadataCipherText,
                'signature' => $signature,
                'client' => [
                    'id' => $this->client->id
                ]
            ]
        ]);

        $item = Item::findOrFail($res->getData()->data->id);

        Storage::disk('cloud')->assertExists($item->path);
    }
}
