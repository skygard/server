<?php

namespace Tests\Unit;

use Mockery;
use Tests\TestCase;
use Skygard\Core\ConfigProvider;
use Skygard\Config\ProviderInterface;
use Skygard\Config\Config;

class ConfigTest extends TestCase
{
    /**
     * Test config providers register correctly
     *
     * @return void
     */
    public function testProvidersRegisterAndExpose()
    {
        // Mock config provider
        $configProvider = Mockery::mock(ProviderInterface::class);

        // Config provider should receive getExposed
        $configProvider->shouldReceive('getExposed')->once()->andReturn([
            'foo'
        ]);

        // Instantiate config class
        $config = new Config;

        // Register core config provider
        $config->register($configProvider);

        // Config provider should receive getConfig
        $configProvider->shouldReceive('getConfig')->once()->andReturn([
            'foo' => 'bar',
            'baz' => 'qux'
        ]);

        // Check only exposed configs are returned from exposed method
        $this->assertEquals([
            'foo' => 'bar'
        ], $config->exposed()->toArray());
    }
}
