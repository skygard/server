<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeypairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keypairs', function (Blueprint $table) {
            $table->uuid('id');

            $table->uuid('user_id');
            $table->uuid('client_id');

            $table->text('encryption_public_key');
            $table->text('encryption_private_key');
            $table->text('signing_public_key');
            $table->text('signing_private_key');
            $table->boolean('revoked')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keypairs');
    }
}
