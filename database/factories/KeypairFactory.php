<?php

use Faker\Generator as Faker;
use Skygard\Keypair;
use phpseclib\Crypt\RSA;

$factory->define(Keypair::class, function (Faker $faker) {
    return [
        'encryption_public_key' => $faker->sha1,
        'encryption_private_key' => $faker->sha1,
        'signing_public_key' => $faker->sha1,
        'signing_private_key' => $faker->sha1,
    ];
});
