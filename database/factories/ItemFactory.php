<?php

use Faker\Generator as Faker;

$factory->define(Skygard\Item::class, function (Faker $faker) {
    return [
        'disk' => 'cloud',
        'path' => $faker->sha1,
        'hash' => $faker->sha256,
        'key' => $faker->sha256,
        'metadata' => $faker->sha256,
        'signature' => $faker->sha256
    ];
});
