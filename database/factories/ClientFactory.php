<?php

use Skygard\Client;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'secret' => Str::random(40),
        'redirect' => 'http://localhost/callback',
        'personal_access_client' => false,
        'password_client' => false,
        'revoked' => false
    ];
});
