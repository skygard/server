<?php

/**
 * @OA\Info(title="Skygard API", version="1.0")
 */

Route::middleware('auth:api')->group(function () {
    /**
     * @OA\Get(
     *     path="/api/user",
     *     @OA\Response(response="200", description="The currenly authenticated user")
     * )
     */
    Route::get('/user', 'UserController@user');

    Route::get('/keypairs', 'KeypairController@keypairs');
    Route::get('/keypairs/{id}', 'KeypairController@getKeypair');
    Route::delete('/keypairs/{id}', 'KeypairController@revokeKeypair');
    Route::post('/keypairs', 'KeypairController@addKeypair');

    Route::get('/items', 'ItemController@list');
    Route::get('/items/{id}', 'ItemController@getItem');
    Route::get('/items/{id}/download', 'ItemController@download');
    Route::post('/items', 'ItemController@create');
    Route::delete('/items/{id}', 'ItemController@delete');

});