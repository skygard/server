<?php

namespace Skygard\Traits;

trait Paginate
{
    /**
     * Query string parameter for pagination page limit
     *
     * @var string
     */
    protected $perPageParam = 'limit';

    /**
     * Default pagination limit if one isn't defined
     *
     * @var integer
     */
    protected $defaultPerPage = 20;

    /**
     * Maximum items per page, regardless of specified limit
     *
     * @var integer
     */
    protected $maxPerPage = 100;

    /**
     * Paginate a query using limits from query string
     *
     * @param Builder $query
     * @return LengthAwarePaginator
     */
    protected function paginate($query)
    {
        return $query->paginate(
            $this->paginationLimit()
        )->appends(
            $this->perPageParam,
            $this->paginationLimit()
        );
    }

    /**
     * Helper method to get pagination limit based on query
     * string param, default and maximum per page.
     *
     * @return integer
     */
    protected function paginationLimit(): int
    {
        return $this->intOrMax(
            request()->get($this->perPageParam) ?: $this->defaultPerPage,
            $this->maxPerPage
        );
    }

    /**
     * Helper that returns the specifid integer if its less
     * than the max, otherwise the max is returned.
     *
     * @param integer $integer
     * @param integer $max
     * @return integer
     */
    protected function intOrMax(int $integer, int $max): int
    {
        return $integer > $max ? $max : $integer;
    }
}