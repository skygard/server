<?php

namespace Skygard\Traits;

use Skygard\User;
use Skygard\Client;

trait BelongsToUserAndClient
{
    /**
     * Client relationship
     *
     * @return BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * User relationship
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope to filter keypairs by current user and client
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeBelongsToCurrentUserAndClient($query)
    {
        $user = request()->user();

        if (! $client = $user->token()->client) {
            return $query->whereRaw('1 = 0'); // Hack to force no results
        }

        return $query->where('user_id', '=', $user->id)->where('client_id', '=', $client->id);
    }
}