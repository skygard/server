<?php

namespace Skygard\Core;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    use UuidModel;
}