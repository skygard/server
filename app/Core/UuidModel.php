<?php

namespace Skygard\Core;

use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

trait UuidModel
{
    // protected $keyType = 'string';
    // public $incrementing = false;
    protected $uuid = true;

    public function __construct(array $attributes = [])
    {
        if ($this->uuid) {
            $this->setKeyType('string');
            $this->setIncrementing(false);
        }

        parent::__construct($attributes);
    }

    protected function performInsert(Builder $query)
    {
        if ($this->uuid) {
            $this->{$this->primaryKey} = Str::uuid();
        }

        return parent::performInsert($query);
    }
}