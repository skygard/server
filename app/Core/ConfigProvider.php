<?php

namespace Skygard\Core;

use Skygard\Config\ProviderInterface;
use Skygard\User;
use Illuminate\Database\QueryException;

class ConfigProvider implements ProviderInterface
{
    /**
     * Get config items for core
     *
     * @return array
     */
    public function getConfig(): array
    {
        return [
            'allow_registration' => env('ALLOW_REGISTRATION', false) or $this->doesUserExist()
        ];
    }

    /**
     * Get exposed config items for use client-side
     *
     * @return array
     */
    public function getExposed(): array
    {
        return [
            'allow_registration'
        ];
    }

    protected function doesUserExist(): bool
    {
        try {
            return User::count() === 0;
        } catch (QueryException $e) {
            return false;
        }
    }
}