<?php

namespace Skygard;

use Laravel\Passport\Client as BaseClient;
use Skygard\Core\UuidModel;

class Client extends BaseClient
{
    use UuidModel;
}