<?php

namespace Skygard;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Skygard\Core\UuidModel;

class User extends Authenticatable
{
    use UuidModel, HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Keys relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function keypairs()
    {
        return $this->hasMany(Keypair::class);
    }
}
