<?php

namespace Skygard\Rules;

use phpseclib\Crypt\RSA;
use Illuminate\Contracts\Validation\Rule;

class ValidSignature implements Rule
{
    protected $publicKey;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?string $publicKey, string $data)
    {
        $this->publicKey = $publicKey;
        $this->data = $data;

        $this->rsa = new RSA;
        $this->rsa->loadKey($this->publicKey);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Decode first
        $signature = base64_decode($value);

        try {
            // Verify signature
            return $this->rsa->verify($this->data, $signature);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid signature.';
    }
}
