<?php

namespace Skygard\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\UploadedFile;

class ValidFileHash implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @param UploadedFile $file
     * @param string $algorithm
     * @return void
     */
    public function __construct(UploadedFile $file, string $algorithm = 'sha256')
    {
        $this->file = $file;
        $this->algorithm = $algorithm;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Get hash of the file
        $hash = hash_file($this->algorithm, $this->file);
 
        return $value == $hash;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid file hash.';
    }
}
