<?php

namespace Skygard;

use Laravel\Passport\AuthCode as BaseAuthCode;
use Skygard\Core\UuidModel;

class AuthCode extends BaseAuthCode
{
    use UuidModel;
}