<?php

namespace Skygard\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Skygard\Rules\ValidSignature;

class AddKeypairRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'encryption.public_key' => ['required', 'max:4000'],
            'encryption.private_key' => ['required', 'max:4000'],
            'signing.public_key' => ['required', 'max:4000'],
            'signing.private_key' => ['required', 'max:4000'],
            'signature' => [
                'required',
                new ValidSignature(
                    $this->input('signing.public_key'),
                    $this->getSignaturePlain()
                )
            ]
        ];
    }

    protected function getSignaturePlain()
    {
        return implode(':', [
            $this->input('encryption.public_key'),
            $this->input('encryption.private_key'),
            $this->input('signing.public_key'),
            $this->input('signing.private_key'),
        ]);
    }
}
