<?php

namespace Skygard\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Skygard\Rules\ValidSignature;
use Skygard\Keypair;
use Skygard\Rules\ValidFileHash;
use Skygard\Rules\ValidKeypair;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $publicKey = Keypair::belongsToCurrentUserAndClient()->find(request()->get('keypair_id'))->signing_public_key ?? '';

        return [
            'keypair_id' => ['required', new ValidKeypair],
            'key' => ['required', 'max:1000'],
            'data' => ['required', 'file'],
            'hash' => ['required', 'size:64', new ValidFileHash($this->file('data'))],
            'metadata' => ['nullable', 'max:1000'],
            'signature' => ['required', 'max:1000', new ValidSignature($publicKey, $this->generatePlainSignature())]
        ];
    }

    /**
     * Generate plain signature
     *
     * @return string
     */
    protected function generatePlainSignature()
    {
        return implode(':', [
            $this->input('key'),
            $this->input('hash'),
            $this->input('metadata')
        ]);
    }
}
