<?php

namespace Skygard\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'keypair' => new KeypairResource($this->keypair),
            'client' => new ClientResource($this->client),
            'key' => $this->key,
            'hash' => $this->hash,
            'metadata' => $this->metadata,
            'signature' => $this->signature
        ];
    }
}
