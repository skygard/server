<?php

namespace Skygard\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KeypairResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'client' => new ClientResource($this->client),
            'encryption' => [
                'public_key' => $this->encryption_public_key,
                'private_key' => $this->encryption_private_key,
            ],
            'signing' => [
                'public_key' => $this->signing_public_key,
                'private_key' => $this->signing_private_key,
            ],
            'revoked' => (bool) $this->revoked,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
