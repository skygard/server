<?php

namespace Skygard\Http\Controllers;

use function OpenApi\scan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class OpenApiController extends Controller
{
    public function serve()
    {
        return Cache::remember('openapi', now()->addHour(), function() {
            return scan(base_path('routes'));
        });
    }
}
