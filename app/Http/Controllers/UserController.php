<?php

namespace Skygard\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get the currently authenticated user
     *
     * @return array
     */
    public function user(Request $request)
    {
        return $request->user();
    }
}
