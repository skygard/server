<?php

namespace Skygard\Http\Controllers;

use Skygard\Keypair;
use Skygard\Traits\Paginate;
use Illuminate\Http\Request;
use Skygard\Http\Resources\KeypairResource;
use Skygard\Http\Requests\AddKeypairRequest;

class KeypairController extends Controller
{
    use Paginate;

    /**
     * Get a list of keys for the current user and client
     *
     * @return Collection
     */
    public function keypairs()
    {
        $keypairs = $this->paginate(
            Keypair::belongsToCurrentUserAndClient()->notRevoked()
        );

        return KeypairResource::collection($keypairs);
    }

    /**
     * Get a single keypair for current user
     *
     * @param string $id
     * @return void
     */
    public function getKeypair($id)
    {
        $keypair = Keypair::belongsToCurrentUserAndClient()
            ->findOrFail($id);

        return new KeypairResource($keypair);
    }

    /**
     * Add a key for the current user and client
     *
     * @param AddKeypairRequest $request
     * @return KeypairResource
     */
    public function addKeypair(AddKeypairRequest $request)
    {
        $data = $request->validated();

        $keypair = $request->user()->keypairs()->create([
            'client_id' => $request->user()->token()->client->id,
            'encryption_public_key' => $data['encryption']['public_key'],
            'encryption_private_key' => $data['encryption']['private_key'],
            'signing_public_key' => $data['signing']['public_key'],
            'signing_private_key' => $data['signing']['private_key']
        ]);

        return new KeypairResource($keypair);
    }

    /**
     * Revoke keypair
     *
     * @param string $id
     * @return void
     */
    public function revokeKeypair($id)
    {
        $keypair = Keypair::belongsToCurrentUserAndClient()->findOrFail($id);

        $keypair->revoked = true;

        $keypair->save();

        return new KeypairResource($keypair);
    }
}
