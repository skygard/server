<?php

namespace Skygard\Http\Controllers;

use Illuminate\Http\Request;
use Skygard\Http\Requests\AddItemRequest;
use Skygard\Item;
use Skygard\Http\Resources\ItemResource;
use Skygard\Traits\Paginate;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    use Paginate;

    /**
     * Create item for the current user
     *
     * @param AddItemRequest $request
     * @return void
     */
    public function create(AddItemRequest $request)
    {
        $data = $request->validated();
        
        $path = $data['data']->store($request->user()->id, 'cloud');

        $item = Item::create([
            'keypair_id' => $data['keypair_id'],
            'user_id' => $request->user()->id,
            'client_id' => $request->user()->token()->client->id,
            'disk' => 'cloud',
            'path' => $path,
            'key' => $data['key'],
            'hash' => $data['hash'],
            'metadata' => $data['metadata'],
            'signature' => $data['signature']
        ]);

        return new ItemResource($item);
    }

    /**
     * Get a single item by id
     *
     * @param string $id
     * @return void
     */
    public function getItem($id)
    {
        $item = Item::belongsToCurrentUserAndClient()->findOrFail($id);

        return new ItemResource($item);
    }

    /**
     * Get a paginated list of user/client items
     *
     * @return void
     */
    public function list()
    {
        $items = $this->paginate(
            Item::belongsToCurrentUserAndClient()
        );

        return ItemResource::collection($items);
    }

    /**
     * Download an item from the filesystem
     *
     * @param string $id
     * @return void
     */
    public function download($id)
    {
        $item = Item::belongsToCurrentUserAndClient()->findOrFail($id);

        return redirect(
            Storage::disk($item->disk)->temporaryUrl(
                $item->path,
                now()->addMinutes(20)
            )
        );
    }

    /**
     * Delete an item by id
     *
     * @param string $id
     * @return void
     */
    public function delete($id)
    {
        $item = Item::belongsToCurrentUserAndClient()->findOrFail($id);

        Storage::disk($item->disk)->delete($item->path);

        return [
            'data' => [
                'id' => $id,
                'deleted' => true
            ]
        ];
    }
}
