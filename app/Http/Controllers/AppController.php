<?php

namespace Skygard\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    public function handle()
    {
        return view('app');
    }
}
