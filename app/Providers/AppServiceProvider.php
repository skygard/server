<?php

namespace Skygard\Providers;

use Illuminate\Support\ServiceProvider;
use Skygard\Config\Config;
use Skygard\Core\ConfigProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register core config provider
        app(Config::class)->register(new ConfigProvider);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Use custom Skygard migrations for passport
        Passport::ignoreMigrations();

        // Register config as a singleton
        $this->app->singleton(Config::class, Config::class);
    }
}
