<?php

namespace Skygard;

use Skygard\Core\Model;
use Skygard\Traits\BelongsToUserAndClient;

class Keypair extends Model
{
    use BelongsToUserAndClient;

    protected $fillable = [
        'client_id',
        'encryption_public_key',
        'encryption_private_key',
        'signing_public_key',
        'signing_private_key',
        'revoked'
    ];

    /**
     * Scope for unrevoked keys
     *
     * @param Builder $query
     * @return void
     */
    public function scopeNotRevoked($query)
    {
        return $query->where('revoked', '=', false);
    }

    /**
     * Scope for revoked keys
     *
     * @param Builder $query
     * @return void
     */
    public function scopeRevoked($query)
    {
        return $query->where('revoked', '=', true);
    }
}
