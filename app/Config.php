<?php

namespace Skygard;

use Illuminate\Support\Facades\Facade;
use Skygard\Config\Config as ConcreteConfig;

class Config extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return ConcreteConfig::class;
    }
}