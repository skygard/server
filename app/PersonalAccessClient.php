<?php

namespace Skygard;

use Laravel\Passport\PersonalAccessClient as BasePersonalAccessClient;
use Skygard\Core\UuidModel;

class PersonalAccessClient extends BasePersonalAccessClient
{
    use UuidModel;
}