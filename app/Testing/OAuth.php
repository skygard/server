<?php

namespace Skygard\Testing;

use Laravel\Passport\Passport;
use Skygard\Client;

class OAuth
{
    public function __construct($user)
    {
        $this->user = $user;
    }

    public static function actingAs(...$args)
    {
        $user = Passport::actingAs(...$args);

        return new static($user);
    }

    public function withClient(Client $client)
    {
        $this->user->token()
            ->shouldReceive('getAttribute')
            ->with('client')
            ->andReturn(
                $client
            );
    }
}