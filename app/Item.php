<?php

namespace Skygard;

use Skygard\Core\Model;
use Skygard\Traits\BelongsToUserAndClient;

class Item extends Model
{
    use BelongsToUserAndClient;

    protected $fillable = [
        'user_id',
        'client_id',
        'keypair_id',
        'disk',
        'path',
        'key',
        'hash',
        'metadata',
        'signature',
    ];

    /**
     * Keypair relationship
     *
     * @return void
     */
    public function keypair()
    {
        return $this->belongsTo(Keypair::class);
    }
}
