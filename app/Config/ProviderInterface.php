<?php

namespace Skygard\Config;

interface ProviderInterface
{
    /**
     * Get all config items for this provider
     *
     * @return array
     */
    public function getConfig(): array;

    /**
     * Get exposed config items for this provider
     *
     * @return array
     */
    public function getExposed(): array;
}