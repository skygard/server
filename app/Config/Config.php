<?php

namespace Skygard\Config;

use Illuminate\Support\Collection;

class Config
{
    /**
     * Have the providers configs been initialized
     *
     * @var boolean
     */
    protected $initialized = false;

    /**
     * Array of registered providers
     *
     * @var array
     */
    protected $providers = [];

    /**
     * Array of exposed config items from all providers
     *
     * @var array
     */
    protected $exposed = [];

    /**
     * Register a config provider
     *
     * @param ProviderInterface $provider
     * @return void
     */
    public function register(ProviderInterface $provider)
    {
        $this->providers[] = $provider;
        $this->exposed = array_merge($this->exposed, $provider->getExposed());

        if ($this->initialized) {
            $this->initializeConfigFor($provider);
        }
    }

    /**
     * Get exposed config items for use client-side
     *
     * @return Collection
     */
    public function exposed(): Collection
    {
        $this->initializeConfigsIfRequired();

        $config = [];

        foreach ($this->exposed as $key) {
            $config[$key] = config($key);
        }

        return new Collection($config);
    }

    /**
     * Initialize configs if the haven't already been initialized
     *
     * @return void
     */
    protected function initializeConfigsIfRequired()
    {
        if ($this->initialized) {
            return;
        }

        $this->initializeConfigs();
    }

    /**
     * Initialize configs for all registered providers
     *
     * @return void
     */
    protected function initializeConfigs()
    {
        foreach ($this->providers as $provider) {
            $this->initializeConfigFor($provider);
        }

        $this->initialized = true;
    }

    /**
     * Initialize configs for a specific provider
     *
     * @param ProviderInterface $provider
     * @return void
     */
    protected function initializeConfigFor(ProviderInterface $provider)
    {
        config(
            $provider->getConfig()
        );
    }
}