import Axios from "axios";
import pbkdf2 from 'pbkdf2'

export function checkLogin() {
  return function (dispatch) {
    Axios.get('/api/user').then(response => {
      dispatch({
        type: 'LOGGED_IN',
        payload: response.data
      })
    }, error => {
      dispatch({
        type: 'NOT_LOGGED_IN',
        payload: error
      })
    })
  }
}