import React, { Component } from 'react'
import AuthLayout from './layout/AuthLayout'
import LoginForm from './forms/LoginForm';

export default class Login extends Component {
  render() {
    return (
      <AuthLayout title="Login">
        <LoginForm />
      </AuthLayout>
    );
  }
}