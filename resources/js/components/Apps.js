import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Layout, Breadcrumb } from 'antd';

const {
  Content
} = Layout;

@connect((store) => {
  return {
    user: store.user
  }
})
export default class Apps extends Component {

  render() {
    return (
      <Content style={{ margin: '0 16px' }}>
        <Breadcrumb style={{ margin: '16px 0' }}>
          <Breadcrumb.Item>Apps</Breadcrumb.Item>
        </Breadcrumb>
        <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
          WIP.
        </div>
      </Content>
    )
  }
}