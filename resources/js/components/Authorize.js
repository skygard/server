import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Layout, Breadcrumb, Button } from 'antd';
import AuthLayout from './layout/AuthLayout';

const {
  Content
} = Layout;

@connect((store) => {
  return {
    user: store.user
  }
})
export default class Authorize extends Component {

  render() {
    return (
      <AuthLayout title={`${window.client.name} would like to access your Skygard server`}>
        <p>
          {window.client.name} is requesting access to your Skygard account.
        </p>

        <p>
          {window.client.name} will only be able to access data created by itself.
          Data from other applications will not be accessible by {window.client.name}.
        </p>

        <form method="post" action="/oauth/authorize" style={{float: 'left'}}>
          <input type="hidden" name="_token" value={document.head.querySelector('meta[name="csrf-token"]').content} />
          <input type="hidden" name="_method" value="DELETE" />

          <input type="hidden" name="state" value={window.state} />
          <input type="hidden" name="client_id" value={window.client.id} />

          <Button htmlType="submit" size="large">Deny</Button>
        </form>

        <form method="post" action="/oauth/authorize" style={{float: 'right'}}>
          <input type="hidden" name="_token" value={document.head.querySelector('meta[name="csrf-token"]').content} />

          <input type="hidden" name="state" value={window.state} />
          <input type="hidden" name="client_id" value={window.client.id} />
          <Button type="primary" htmlType="submit" size="large">Authorize</Button>
        </form>

      </AuthLayout>
    )
  }
}