import { Layout, Row, Col, Card } from 'antd';
import React, { Component } from 'react';

const { Header, Content } = Layout;

export default class AuthLayout extends Component {
  render() {
    return (
      <Layout>
        <Header style={{margin: '20px', textAlign: 'center', background: '#f0f2f5'}}>
          <img style={{maxHeight: '100%'}} src="/img/skygard-small.png" />
        </Header>
        <Content style={{ padding: '0 24px', minHeight: 280 }} className="main-layout-content">
          <Row type="flex" justify="center" align="middle">
            <Col span={6}>
              <Card title={this.props.title} bordered={false} style={{ width: "100%" }}>
                  {this.props.children}
              </Card>
            </Col>
          </Row>
        </Content>
      </Layout>
    );
  }
}