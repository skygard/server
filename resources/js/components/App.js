import React, { Component } from 'react';
import Login from './Login';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { checkLogin } from '../actions/userActions';
import { connect } from 'react-redux'
import { Layout, Menu, Icon } from 'antd';
import MainMenu from './MainMenu';
import SubMenu from 'antd/lib/menu/SubMenu';
import Dashboard from './Dashboard';
import Apps from './Apps';
import Users from './Users';
import Settings from './Settings';
import Authorize from './Authorize';

const {
  Header, Footer, Sider
} = Layout;

@connect((store) => {
  return {
    user: store.user
  }
})
export default class App extends Component {
  componentDidMount() {
    // Dispatch a check login action
    this.props.dispatch(checkLogin())
  }

  logout() {
    window.location.href = '/logout'
  }

  render() {
    if (! this.props.user.loginChecked) {
      return null;
    } else if (! this.props.user.loggedIn) {
      return (
        <Router>
          <Route component={Login} />
        </Router>
      )
    } else if (window.authorize) {
      return (
        <Router>
          <Route component={Authorize} />
        </Router>
      )
    }

    return (
      <Router>
        <Layout style={{ minHeight: '100vh' }}>
          <Sider>
            <div style={{
              display: 'flex',
              justifyContent: 'center',
              padding: '10px'
            }}>
              <img src="/img/skygard-light.png" style={{width: "100px"}} />
            </div>
            <MainMenu />
          </Sider>
          <Layout>
            <Header style={{ background: '#fff', padding: 0, display: 'flex', justifyContent: 'right' }}>
              <Menu
                mode="horizontal"
                style={{ lineHeight: '64px', border: 0 }}
              >
                <SubMenu title={<span><Icon type="user" />{this.props.user.user.name}</span>}>
                    <Menu.Item>Settings</Menu.Item>
                    <Menu.Item onClick={this.logout.bind(this)}>Logout</Menu.Item>
                </SubMenu>
              </Menu>
            </Header>

            <Switch>
              <Route path="/" exact component={Dashboard} />
              <Route path="/apps" exact component={Apps} />
              <Route path="/users" exact component={Users} />
              <Route path="/settings" exact component={Settings} />
            </Switch>

            <Footer style={{ textAlign: 'center' }}>
              Skygard is an open source project licensed under the AGPLv3
            </Footer>
          </Layout>
        </Layout>
      </Router>
    )
  }
}