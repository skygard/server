import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
import { Link, withRouter } from 'react-router-dom'

class MainMenu extends Component {
  render() {
    const key = this.props.location.pathname.split("/", 2).join('/')

    return (
      <Menu theme="dark" defaultSelectedKeys={[key]} mode="inline">
        <Menu.Item key="/">
          <Link to="/">
            <Icon type="dashboard" />
            <span>Dashboard</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/apps">
          <Link to="/apps">
            <Icon type="plus" />
            <span>Apps</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/users">
          <Link to="/users">
            <Icon type="user" />
            <span>Users</span>
          </Link>
        </Menu.Item>
        <Menu.Item key="/settings">
          <Link to="settings">
            <Icon type="setting" />
            <span>Settings</span>
          </Link>
        </Menu.Item>
      </Menu>
    )
  }
}

export default withRouter(props => <MainMenu {...props} />)