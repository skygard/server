import React, { Component } from 'react'
import { Form, Button, Input, Icon } from 'antd';
import pbkdf2 from 'pbkdf2'

class LoginForm extends Component {
  state = {
    email: '',
    password: '',
    derivedKey: ''
  }

  onChange(e) {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit(e) {
    this.props.form.validateFields((err, values) => {
      if (err) {
        e.preventDefault()
        return
      }

      this.setState({
        derivedKey: pbkdf2.pbkdf2Sync(
          this.state.password,
          this.state.email,
          5000,
          64,
          'sha256'
        ).toString('hex')
      })
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit.bind(this)} action="/login" method="post">
        <Form.Item>
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email address!' }],
          })(
            <Input
              id="email"
              onChange={this.onChange.bind(this)}
              value={this.state.email}
              name="email"
              size="large"
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Email address"
            />
          )}
        </Form.Item>

        <Form.Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password!' }],
          })(
            <Input
              id="password"
              onChange={this.onChange.bind(this)}
              value={this.state.password}
              type="password"
              size="large"
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Password"
            />
          )}
        </Form.Item>

        <Input name="password" type="hidden" value={this.state.derivedKey} />
        <Input name="_token" type="hidden" value={document.head.querySelector('meta[name="csrf-token"]').content} />

        <Button type="primary" htmlType="submit" style={{width: '100%'}}>Login</Button>
      </Form>
    )
  }
}

export default Form.create()(LoginForm)