import { combineReducers } from 'redux'
import { withReduxStateSync } from 'redux-state-sync'
import userReducer from './userReducer'

export default withReduxStateSync(combineReducers({
  user: userReducer
}))