export default function userReducer(state={loginChecked: false, loggingIn: false, loggedIn: false, user: null}, action) {

  switch (action.type) {
    case 'NOT_LOGGED_IN':
      return {...state, loginChecked: true, loggingIn: false, error: action.payload, user: null}

    case 'LOGGED_IN':
      return {...state, loginChecked: true, loggingIn: false, loggedIn: true, user: action.payload}
  }

  return {...state}
}