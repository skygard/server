import './bootstrap'
import App from './components/App'
import ReactDOM from 'react-dom'
import React from 'react'
import { Provider } from 'react-redux'
import store from './store';

if (document.getElementById('root')) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById('root')
  )
}