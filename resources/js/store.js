import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk';
import reducers from './reducers'
import { createStateSyncMiddleware, initStateWithPrevTab } from 'redux-state-sync';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, composeEnhancers(
  applyMiddleware(...[
    thunk,
    createStateSyncMiddleware({
      channel: 'skygard',
      broadcastChannelOption: {type: 'localstorage' }
    }),
  ])
))

initStateWithPrevTab(store)

export default store