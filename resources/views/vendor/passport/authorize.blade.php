@extends('app')

@push('data')
  window.authorize = true
  window.scopes = {!! json_encode($scopes) !!}
  window.client = {!! json_encode($client->only('id', 'name')) !!}
  window.state = {!! $request->state ?: "''" !!}
@endpush