<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ mix('css/app.css') }}" rel="stylesheet">

        <script>
          window.config = {!! Skygard\Config::exposed() !!}

          @stack('data')
        </script>

        <title>Skygard</title>
    </head>
    <body style="background-color: #f0f2f5">
        <div id="root">
          
        </div>

    <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
