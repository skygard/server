# Set the base image to latest Ubuntu LTS
FROM ubuntu:18.04

# Update package list
RUN apt-get update

# Install PHP (and extensions), composer, node, npm, etc
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qq curl gnupg php composer nodejs npm php-zip php-mysql php-xml php-mbstring php-dom

# Add yarn repo
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# Update package list (again)
RUN apt-get update

# Install yarn
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qq yarn

# Clear out the local repository of retrieved package files
RUN apt-get clean
