# Skygard Server
[![pipeline status](https://gitlab.com/skygard/server/badges/master/pipeline.svg)](https://gitlab.com/skygard/server/pipelines)
[![coverage report](https://gitlab.com/skygard/server/badges/master/coverage.svg)](https://gitlab.com/skygard/server/commits/master)

API server component for Skygard.

## Contribution guidelines

All contributions to this repository are considered to be
licensed under the AGPLv3 or any later version.

Skygard does not require a CLA (Contributor License Agreement).
The copyright belongs to all the individual contributors. Therefore we recommend
that every contributor adds following line to the AUTHORS file, when submitting a
Pull Request:

```
- Your Name <your email address>
```
